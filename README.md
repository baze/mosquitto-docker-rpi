# Mosquitto MQTT-broker in Docker container for Raspian Jessie.


Mosquitto is preconfigured to connect another MQTT-broker where sensor-topic is syncronized.
MQTT broker can also be started to persist messages outside running container.

# Examples


## Example 1) Start MQTT broker in port 1883
```
docker run -d -p 1883:1883 registry.gitlab.com/baze/mosquitto-docker-rpi
```

You can test by starting to listen for topic:
```
mosquitto_sub  -h localhost -p 1883 -v -t 'test/topic'
```

Then publish message to that same topic with this:
```
mosquitto_pub -h localhost -p 21883 -t 'test/topic' -m 'helloWorld'
```

## Example 2) Start MQTT Broker and persist messages database outside the container to ensure you don't lose messages if broker is restarted.
```
docker run -d -p 1883:1883 \
        --volume /path/where/messages/are/saved:/mosquitto_data \
        registry.gitlab.com/baze/mosquitto-docker-rpi
```

## Example 3) Start MQTT Broker and set login credentials for clients
```
docker run -d -p 1883:1883 \
        -e MQTT_LOGIN_USERNAME=username -e MQTT_LOGIN_PASSWORD=password \
        registry.gitlab.com/baze/mosquitto-docker-rpi
```

## Example 4) Start MQTT broker and enable MQTT bridging to another MQTT-broker.
```
sudo docker run -d \
        --volume /path/where/message_db/is/saved:/mosquitto_data \
        -e MQTT_HOST=remote_mqtt.host.com:1883 \
        -e MQTT_CLIENTID=your_id \
        -e MQTT_USERNAME=your_username_to_remote_mqtt_broker \
        -e MQTT_PASSWORD=your_password_to_remote_mqtt_broker \
        -e MQTT_TOPIC=sensor \
        -p 1883:1883 \
        registry.gitlab.com/baze/mosquitto-docker-rpi
```
This means messages sent to topic **"sensor/"** are synched to remote MQTT.
